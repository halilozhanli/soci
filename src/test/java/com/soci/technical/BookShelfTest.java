package com.soci.technical;


import com.soci.technical.builder.BookBuilder;
import com.soci.technical.builder.BookShelfBuilder;
import com.soci.technical.builder.MagazineBuilder;
import com.soci.technical.builder.NotebookBuilder;
import com.soci.technical.model.Book;
import com.soci.technical.model.BookShelf;
import com.soci.technical.model.Magazine;
import com.soci.technical.model.Notebook;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class BookShelfTest {

    @Test
    public void testBookShelfBuild() {
        Integer capacity = 4;
        final BookShelf shelf = BookShelfBuilder.build(capacity);
        assertEquals(shelf.getTotalCapacity(), capacity);
        assertEquals(shelf.getAvailableCapacity(), capacity);
        assertEquals(shelf.getUsedCapacity(), 0);
    }

    @Test
    public void testBookShelfStoreSuccess() {
        Integer capacity = 4;
        final BookShelf shelf = BookShelfBuilder.build(capacity);
        final Book book1 = BookBuilder.build(1);
        final Book book2 = BookBuilder.build(2);
        shelf.store(book1);
        assertEquals(shelf.getTotalCapacity(), capacity);
        assertEquals(shelf.getAvailableCapacity(), capacity - 1);
        assertEquals(shelf.getUsedCapacity(), 1);
        shelf.store(book2);
        assertEquals(shelf.getTotalCapacity(), capacity);
        assertEquals(shelf.getAvailableCapacity(), capacity - 2);
        assertEquals(shelf.getUsedCapacity(), 2);
    }

    @Test
    public void testBookShelfRetrieveSuccess() {
        Integer capacity = 4;
        final BookShelf shelf = BookShelfBuilder.build(capacity);
        final Book book1 = BookBuilder.build(1);
        final Book book2 = BookBuilder.build(2);
        final Magazine magazine1 = MagazineBuilder.build(1);
        final Notebook notebook1 = NotebookBuilder.build(1);
        shelf.store(book1);
        shelf.store(book2);
        shelf.store(magazine1);
        shelf.store(notebook1);
        shelf.retrieve(book1);
        assertEquals(shelf.getTotalCapacity(), capacity);
        assertEquals(shelf.getAvailableCapacity(), capacity - 3);
        assertEquals(shelf.getUsedCapacity(), 3);
    }

    @Test
    public void testBookPageContentSuccess() {
        final int id = 1;
        final int pageNumber = 1;
        final String content = "Book" + id + ", Page: " + pageNumber;
        final Book book1 = BookBuilder.build(id);
        assertEquals(book1.getPageContent(pageNumber), content);
    }

    @Test
    public void testBookPageContentNotExists() {
        final int id = 1;
        final int pageNumber = 6;
        final String content = "Book" + id + ", Page: " + pageNumber;
        final Book book1 = BookBuilder.build(id);
        assertNotEquals(book1.getPageContent(pageNumber), content);
    }

    @Test
    public void testNotebookPageContentSuccess() {
        final int id = 1;
        final int pageNumber = 1;
        final String content = "Notebook" + id + ", Page: " + pageNumber;
        final Notebook notebook = NotebookBuilder.build(id);
        assertEquals(notebook.getPageContent(pageNumber), content);
    }

    @Test
    public void testNotebookPageContentNotExists() {
        final int id = 1;
        final int pageNumber = 6;
        final String content = "Notebook" + id + ", Page: " + pageNumber;
        final Notebook notebook = NotebookBuilder.build(id);
        assertNotEquals(notebook.getPageContent(pageNumber), content);
    }

    @Test
    public void testMagazinePageContentSuccess() {
        final int id = 1;
        final int pageNumber = 1;
        final String content = "Magazine" + id + ", Page: " + pageNumber;
        final Magazine magazine = MagazineBuilder.build(id);
        assertEquals(magazine.getPageContent(pageNumber), content);
    }

    @Test
    public void testMagazinePageContentNotExists() {
        final int id = 1;
        final int pageNumber = 6;
        final String content = "Magazine" + id + ", Page: " + pageNumber;
        final Magazine magazine = MagazineBuilder.build(id);
        assertNotEquals(magazine.getPageContent(pageNumber), content);
    }

    @Test
    public void testBookShelfStoreException() {
        Integer capacity = 1;
        final BookShelf shelf = BookShelfBuilder.build(capacity);
        final Book book1 = BookBuilder.build(1);
        final Book book2 = BookBuilder.build(2);
        shelf.store(book1);
        Assertions.assertThrows(RuntimeException.class, () -> {
            shelf.store(book2);
        });
    }

    @Test
    public void testBookShelfRetrieveException() {
        Integer capacity = 1;
        final BookShelf shelf = BookShelfBuilder.build(capacity);
        final Book book1 = BookBuilder.build(1);
        Assertions.assertThrows(RuntimeException.class, () -> {
            shelf.retrieve(book1);
        });
    }

}
