package com.soci.technical.builder;

import com.soci.technical.model.Book;

public class BookBuilder {

    public static Book build(Integer id) {
        Book book = new Book("Book" + 1, "Book" + 1);
        book.addContent(1, "Book" + id + ", Page: 1");
        book.addContent(2, "Book" + id + ", Page: 2");
        book.addContent(3, "Book" + id + ", Page: 3");
        book.addContent(4, "Book" + id + ", Page: 4");
        return book;
    }

}
