package com.soci.technical.builder;

import com.soci.technical.model.BookShelf;

public class BookShelfBuilder {

    public static BookShelf build(Integer capacity) {
        return new BookShelf(capacity);
    }

}
