package com.soci.technical.builder;

import com.soci.technical.model.Magazine;

public class MagazineBuilder {

    public static Magazine build(Integer id) {
        Magazine magazine = new Magazine("Magazine" + id);
        magazine.addContent(1, "Magazine" + id + ", Page: 1");
        return magazine;
    }

}
