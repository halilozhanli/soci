package com.soci.technical.builder;

import com.soci.technical.model.Notebook;

public class NotebookBuilder {

    public static Notebook build(Integer id) {
        Notebook notebook = new Notebook("Notebook" + id);
        notebook.addContent(1, "Notebook" + id + ", Page: 1");
        notebook.addContent(2, "Notebook" + id + ", Page: 2");
        return notebook;
    }

}
