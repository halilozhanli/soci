package com.soci.technical.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Notebook extends Printable {

    private String owner;

    public Notebook(String owner) {
        this.owner = owner;
    }

}
