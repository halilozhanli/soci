package com.soci.technical.model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class BookShelf {

    private int capacity;
    private List<Printable> printables = new ArrayList<>();

    public BookShelf(int capacity) {
        this.capacity = capacity;
    }

    public void store(Printable printable) {
        if (printables.size() >= capacity)
            throw new RuntimeException("Invalid store request");
        printables.add(printable);
    }

    public Printable retrieve(Printable printable) {
        final boolean removed = printables.remove(printable);
        if (removed)
            return printable;
        throw new RuntimeException("Invalid remove request");
    }

    public int getTotalCapacity() {
        return this.capacity;
    }

    public int getUsedCapacity() {
        return this.printables.size();
    }

    public int getAvailableCapacity() {
        return this.capacity - printables.size();
    }

}
