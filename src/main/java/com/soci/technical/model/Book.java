package com.soci.technical.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Book extends Printable {

    private String title;
    private String author;

    public Book(String title, String author) {
        this.title = title;
        this.author = author;
    }

}
