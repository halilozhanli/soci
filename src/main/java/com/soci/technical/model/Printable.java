package com.soci.technical.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
public abstract class Printable {

    private Map<Integer, String> content = new HashMap<>();

    public void addContent(@NonNull Integer pageNumber, @NonNull String pageContent) {
        content.put(pageNumber, pageContent);
    }

    public void removeContent(@NonNull Integer pageNumber) {
        content.remove(pageNumber);
    }

    public String getPageContent(Integer pageNumber) {
        return content.getOrDefault(pageNumber, "Page is not exists.");
    }

}
