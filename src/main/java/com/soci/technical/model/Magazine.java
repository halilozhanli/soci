package com.soci.technical.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Magazine extends Printable {

    private String name;

    public Magazine(String name) {
        this.name = name;
    }

}
